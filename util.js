//
// URL Parameter functions
//
function getParameterByName(name, url) {
	if (!url)
		url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function setParameterByName(name, value, url) {
	if (!url)
		url = window.location;
	name = escape(name);
	value = escape(value);
	var nvp = url.search.substr(1).split('&');
	if (nvp == '') {
		url.search = '?' + name + '=' + value;
	} else {
		var i = nvp.length;
		var x;
		while (i--) {
			x = nvp[i].split('=');
			if (x[0] === name) {
				x[1] = value;
				nvp[i] = x.join('=');
				break;
			}
		}
		if (i < 0) {
			nvp[nvp.length] = [ name, value ].join('=');
		}
		url.search = nvp.join('&');
	}
}

function removeParameterByName(name, url){
	if (!url)
		url = window.location;
    var urlParts= url.href.split('?');
    if (urlParts.length > 1) {
        var urlBase = urlParts.shift();
        var queryString = urlParts.join('?');
        var key= encodeURIComponent(name) + '=';
        var params= queryString.split(/[&;]/g);
        //reverse iteration as may be destructive
        for (var i = params.length; i-- > 0;) {
            if (params[i].lastIndexOf(key, 0) !== -1) {
                params.splice(i, 1);
            }
        }
        url = urlBase + (params.length > 0 ? '?' + params.join('&') : '');
        window.location = url;
    }
}

function getAllParameters(url) {
    if (!url)
		url = window.location;
    return url.search.substr(1).split('&');
}

function numberOfParameters(url) {
    var nvp = getAllParameters(url);
	if (nvp == '') {
		return 0;
	} else {
		return nvp.length;
	}
}

function clearParameters(url){
    if (!url)
        url = window.location;
    window.location = url.href.split('?')[0];
}

function addCurrentParametersTo(url) {
	if (!url)
		return;
	var parameters = window.location.search.substr(1).split('?');
	if (parameters[0] !== '') {
		return url + '?' + parameters;
	} else {
		return url;
	}
}



//
// Cookie functions
//
function setCookie(name, value, exHours) {
    if (!exHours)
        exHours=1;
    var d = new Date();
    d.setTime(d.getTime() + (exHours*60*60*1000));
    var expires = "expires="+ d.toUTCString();
		var cookieValue = name + "=" + value + ";" + expires + ";path=/";
    document.cookie = cookieValue;
}

function getCookie(name) {
    var name = name + "=";
    var allCookies = getAllCookies();
    for(var i = 0; i < allCookies.length; i++) {
        var cookie = allCookies[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1);
        }
        if (cookie.indexOf(name) == 0) {
            return cookie.substring(name.length, cookie.length);
        }
    }
    return "";
}

function getAllCookies(){
    return (decodeURIComponent(document.cookie)).split(";");
}

function deleteCookie(name){
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC;';
}

function clearCookies() {
    var cookies = getAllCookies();

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        deleteCookie(name);
    }
}

function countCookies() {
    var cookies = getAllCookies();
    if(cookies[0]==''){
        return 0;
    } else {
		    return cookies.length;
		}
}
